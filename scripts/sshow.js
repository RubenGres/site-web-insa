var indexSlide = 0;
var nbSlide;
var listeImage;
var intervalle;

function initShow(){
    listeImage=document.getElementsByClassName("Images");
    nbSlide=listeImage.length;
    listeImage[0].style.display = "block";
    intervalle = setInterval("changerSlide(1)", 10000);
}

function  resetInterval(){
    clearInterval(intervalle);
    intervalle = setInterval("changerSlide(1)", 10000);
}

function changerSlide(n) 
{
    resetInterval();
    indexSlide += n;
    if (indexSlide  >= nbSlide)
        indexSlide=0;
    if (indexSlide < 0)
        indexSlide= nbSlide-1;
    afficherSlide(indexSlide);
}

function afficherSlide()
{
    var i=0;
    for (i=0 ; i < nbSlide ; i++)
    {
        if (i!=indexSlide)
            listeImage[i].style.display="none";
        else
            listeImage[i].style.display="block";
    }
}
