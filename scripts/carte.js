// déclaration des variables
var defLattitude = 25.0844009;
var defLongitude = 15.4898435;
var defZoom = 1;
var carte = null;
var attribution = 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>';

// liste des villes où l'on ajoutera un marqueur, format nom, lattitude, longitude, url de la page de l'université
var villes = [
    {
        nom: "Seoul",
        lat: 37.4603759,
        long: 126.9519697,
        url: "./universites/seoul.html"
    },

    {
        nom: "MIT",
        lat: 42.359,
        long: -71.092,
        url: "./universites/mit.html"
    },

    {
        nom: "Université libre de Bruxelles",
        lat: 50.850,
        long: 4.34,
        url: "./universites/bruxelles.html"
    },

    {
        nom: "Poudlard",
        lat: 51.4136,
        long: 0.2511,
        url: "./universites/poudlard.html"
    }
]

// initialisation de la carte et de ses paramètres
function initMap() {
    carte = L.map('map', { maxBoundsViscosity: 0 });
    // on precise a leaflet qu'on recupere les donnees sur open street map
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        attribution: attribution,
        minZoom: 2,
        maxZoom: 15,
    }).addTo(carte);

    var southWest = L.latLng(84.930582, -179.606823);
    var northEast = L.latLng(-84.938333, 178.443583);
    bounds = L.latLngBounds(southWest, northEast);
    carte.setMaxBounds(bounds);

    carte.setView([defLattitude, defLongitude], defZoom);
}

// crée pour chaque ville de la liste villes un marqueur
function initMarkers() {
    for (let i = 0; i < villes.length; i++) {
        ville = villes[i];
        addMarker(ville.nom, ville.lat, ville.long, ville.url);
    }
}

// permet d'ajouter un marqueur cliquable qui renvoie sur la page de la ville
function addMarker(nom, lat, long, url) {
    var marker = L.marker([lat, long], { univ_url: url });
    marker.on('click', onClick);

    marker.bindTooltip(nom,
        {
            permanent: false,
            direction: 'top'
        });

    marker.addTo(carte);
}

// fonction permettant d'ouvrir une fenêtre, s'active en cliquant ("_self"=dans la même fenêtre)
function onClick() {
    window.open(this.options.univ_url, "_self");
}

// appel des fonctions d'initialisations de la carte et des marqueurs
window.onload = loadMap;

function loadMap(){
    initMap();
    initMarkers();
}

function centrerZoomer(lat, long, zoom){
    defLattitude = lat;
    defLongitude = long;
    defZoom = zoom;
}

function removeAttribution(){
    attribution = '';
}