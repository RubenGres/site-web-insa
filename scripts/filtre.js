function filtrepays(){
		/*
			Retourne la liste des universités correspondant au pays selectionné
		*/

		// Recupere le pays sélectionné
		var elem = document.getElementById("filtrepays");
		var pays = elem.options[elem.selectedIndex].value;
		
		var univs = document.getElementsByClassName("universite");
		
		var liste = [];	
		
		// Pour chaque universités ..
		Array.from(univs).forEach(
			function(element, index, array){
				// .. on récupere les infos ..
				var inner = element.getElementsByClassName("infos")[0].innerHTML;
				// .. et on l'ajoute a la liste si le pays demandé est dans les infos
				if(!(inner.indexOf(pays) == -1 && pays != "all")){
					liste.push(element);
				}
			}
		)

		return liste;
		
	}

function filtrelangue(){
		/*
			Retourne la liste des universités correspondant au pays selectionné
		*/

		// Recupere le pays sélectionné
		var elem = document.getElementById("filtrelangue");
		var pays = elem.options[elem.selectedIndex].value;
		
		var univs = document.getElementsByClassName("universite");
		
		var liste = [];	
		
		// Pour chaque universités ..
		Array.from(univs).forEach(
			function(element, index, array){
				// .. on récupere les infos ..
				var inner = element.getElementsByClassName("infos")[0].innerHTML;
				// .. et on l'ajoute a la liste si le pays demandé est dans les infos
				if(!(inner.indexOf(pays) == -1 && pays != "all")){
					liste.push(element);
				}
			}
		)

		return liste;
		
	}


function filtrepo(){

		/*
			Retourne la liste des universités correspondant à la PO selectionné
		*/

	// Recupere la PO selectionné
	var elem = document.getElementById("filtrepo");
	var po = elem.options[elem.selectedIndex].value;
	
	var univs = document.getElementsByClassName("universite");
	
	var liste = [];

	// Pour chaque univesités ..
	Array.from(univs).forEach(
		function(element, index, array){
			// .. on recupere ses infos ..
			var inner = element.getElementsByClassName("infos")[0].innerHTML;
			// .. et on l'ajoute a la liste si la PO demandé est dans les infos
			if(!(inner.indexOf(po) == -1 && po != "all")){
				liste.push(element);
			}

			
		}
	);

	return liste;
	
}




function filtre(){

		/*
			Affiche les universités correspondant aux criteres 
		*/

	listePO = filtrepo();
	listePays = filtrepays();
	listeLangue = filtrelangue();


	var univs = document.getElementsByClassName("universite");

	// Universités presentent dans les trois listes
	var commonValues = listePO.filter(function(value) { 
                                   return listePays.indexOf(value) > -1;
							   });

	var commonValues = commonValues.filter(function(value) { 
                                   return listeLangue.indexOf(value) > -1;
							   });
	
	// Pour chaques universités ...
	Array.from(univs).forEach(
		function(element, index, array){
			// ... on la masque ...
			element.style.display = 'none'
			var inner = element.getElementsByClassName("infos")[0].innerHTML;
			// ... et on la réaffiche si elle fait parti des universités demandés
			if(commonValues.indexOf(element) != -1){
				element.style.display = 'block';
			}

			
		}
	);
	console.log(""+commonValues.length);
	// Affichage d'un message d'erreur si la recherche ne renvoie aucun résultat
	if(commonValues.length == 0){
		document.getElementById("pasDeResultat").style.display = 'block';
	} else{
		document.getElementById("pasDeResultat").style.display = 'None';
	}
}