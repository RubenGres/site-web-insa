const pinataModalBoxId = 'pinataModalBox';
const fullscreenPinataId = 'fullscreenPinata';
const pinataHitboxId = 'pinataHitbox';

// Konami code key sequence sequence to enter
const keySequence = [
	'ArrowUp',
	'ArrowUp',
	'ArrowDown',
	'ArrowDown',
	'ArrowLeft',
	'ArrowRight',
	'ArrowLeft',
	'ArrowRight',
	'b',
	'a'
];

let sequenceIndex = 0;
let nbHits = 0;

// Launch after the document has loaded
function initPinata() {
	console.log("wow");
	document.addEventListener('keydown', function (keyDownProperties) {
		if (keyDownProperties.key === keySequence[sequenceIndex++]) {
			// If sequence is correctly entered, display the pinata
			if (sequenceIndex === keySequence.length) {
				showPinata();
				sequenceIndex = 0;
			}
		} else {
			sequenceIndex = 0;
		}
	});
};

function showPinata(){
	insertPinataModalBox();
	let pinata = document.getElementById("pinata");


	let pinataHitbox = document.createElement('div');
	pinataHitbox.setAttribute('id', pinataHitboxId);

	pinata.append(pinataHitbox);
	pinataHitbox.addEventListener('click', () => {
			document.getElementById(pinataModalBoxId).style.display = "initial";
			pinataHitbox.parentNode.removeChild(pinataHitbox);
	});
}

// Add modal box to document
function insertPinataModalBox(){
    let pinataModalBox = document.createElement('div');
    pinataModalBox.setAttribute('id', pinataModalBoxId);

    let fullscreenPinata = document.createElement('div');
    fullscreenPinata.setAttribute('id', fullscreenPinataId);
    fullscreenPinata.addEventListener('click', hitPinata);

    pinataModalBox.prepend(fullscreenPinata);
    document.body.prepend(pinataModalBox);
}

// Remove modal box
function removePinataModalBox() {
	document.getElementById(pinataModalBoxId).remove()
}

// Called when the fullscreen pinata is clicked
function hitPinata() {
	// For the pinata to explode it needs to be hit 10 times
	if (nbHits < 10) {
		// Pinata moves on every hit
		let fullscreenPinata = document.getElementById(fullscreenPinataId);
		fullscreenPinata.style.transform = "rotate("+0.02+"turn) translate("+ 20 +"px,0px)";
		setTimeout(function(){
			fullscreenPinata.style.transform = "rotate("+-0.02+"turn) translate("+ -20 +"px,0px)";
		}, 100);
		nbHits++;
	} else {
		// Throws confetti when the pinata explodes
		tremble();
		generateConfetti();
		setTimeout(removePinataModalBox, 5000);
		nbHits = 0;
	}
}

// Tremble pinata
function tremble(){
	let fullscreenPinata = document.getElementById(fullscreenPinataId);
	setInterval(() => {
		let direction = rand(0, 3);
		let x = Math.random(-1,1);
		let y = Math.random(-1,1);

		fullscreenPinata.style.transform = "translate("+ 50*x +"px,"+ 50*y +"px)";
	}, 25);
}

// Generate confetti for the explosion
function generateConfetti() {
    const container = document.createElement('div');
    container.setAttribute('id', 'pinataConfettiContainer');
    container.style.position = 'absolute';
    container.style.top = "0";
    container.style.left = "0";
    for (let i = 0; i < 400; i++) {
        let t = 1 + .01*rand(0, 100);
        // Creating the confetti as the specified element type
        const element = document.createElement('div');

        element.setAttribute("class", 'particle');
        element.style.transform = "translate(" + rand(0, 100) + "vw, " + rand(0, 100) + "vh)";
        element.style.background = "hsl(" + rand(0, 360) + ", 100%, 65%)";
        element.style.animationDuration = t + "s";
        element.style.animationDelay = -0.1*rand(0, 100)*t + "s";
        // Inserts the confetti into the container
        container.appendChild(element);
    }

    document.getElementById(pinataModalBoxId).appendChild(container);
}

// Random integer
function rand(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
